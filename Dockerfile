FROM python:3.10
RUN pip install pytest
COPY . /app/
ENTRYPOINT [ "python" ]
CMD ["app.py"]
